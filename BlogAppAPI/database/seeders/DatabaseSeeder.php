<?php

namespace Database\Seeders;

use App\Models\Author;
use App\Models\Blog;
use App\Models\Category;
use App\Models\Tag;
use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();
        User::flushEventListeners();
        Category::flushEventListeners();
        Tag::flushEventListeners();
        Blog::flushEventListeners();
        Author::flushEventListeners();

        if(App::environment() === 'production') exit();

        DB::statement('SET FOREIGN_KEY_CHECKS = 0');

        // 1. Truncate all the tables
        $tables = DB::select('SHOW TABLES');
        $tempTableKey = "Tables_in_" . env("DB_DATABASE");
        foreach ($tables as $table) {
            if($table->$tempTableKey != "migrations") {
                DB::table($table->$tempTableKey)->truncate();
            }
        }

        // 2. Set the count of data you want
        $numOfUsers = 20;
        $numOfCategories = 40;
        $numOfBlogs = 40;
        $numOfTags = 1000;

        // 3. Seed the data with the count given

        Tag::factory()->count($numOfTags)->create();
        Category::factory()->count($numOfCategories)->create();
        User::factory()->count($numOfUsers)->create();

        Blog::factory()->count($numOfBlogs)->create();


    }
}
