<?php

namespace Database\Factories;

use App\Models\Blog;
use App\Models\Category;
use App\Models\Tag;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

class BlogFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Blog::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'title' => $this->faker->word(),
            'date' => $this->faker->date('Y-m-d'),
            'views' => $this->faker->numberBetween(1000,2000),
            'rating' => $this->faker->numberBetween(0.0,5.0),
            'image' => $this->faker->randomElement(['1.jpg','2.jpg','3.jpg']),
            'author_id' => User::all()->random()->id,
            'category_id' => Category::all()->random()->id,
            'tag_id' => Tag::all()->random()->id,
            'description' => $this->faker->paragraph(1),
        ];

    }
}
