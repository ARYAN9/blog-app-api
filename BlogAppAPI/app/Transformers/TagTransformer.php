<?php

namespace App\Transformers;

use App\Models\Tag;
use League\Fractal\TransformerAbstract;

class TagTransformer extends TransformerAbstract
{
    /**
     * List of resources to automatically include
     *
     * @var array
     */
    protected $defaultIncludes = [
        //
    ];

    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = [
        //
    ];

    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(Tag $tag)
    {
        return [
            'identifier' => (int) $tag->id,
            'tag_name' =>  $tag->name,

            'links' => [
                [
                    'rel' => 'self',
                    'href' => route('Tags.show', $tag->id)
                ],
            ]
        ];
    }

    public static function getOriginalAttribute(string $transformedAttribute)
    {
        $attributes = [
            'identifier' => 'id',
            'tag_name' =>  'name',
        ];

        return $attributes[$transformedAttribute] ?? null;
    }

    public static function getTransformedAttribute(string $transformedAttribute)
    {
        $attributes = [
             'id' => 'identifier',
             'name' => 'tag_name'
        ];

        return $attributes[$transformedAttribute] ?? null;
    }
}
